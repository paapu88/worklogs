"""
pip3 install --user pandas xlrd
pip3 install --user openpyxl
pip3 install --user styleframe
Usage
python3 worklogs.py 2019 4 5 "Study of word recognition" 50 "Integrating neural nets to Duranc engine" 45 "Integrating analysers to Duranc engine"
"""
import sys
import datetime
import holidays
import calendar
import pandas as pd
import json
from collections import OrderedDict
from styleframe import StyleFrame

with open('data.json') as f:
    basicData = json.load(f, object_pairs_hook=OrderedDict)
current = datetime.date(int(sys.argv[1]), int(sys.argv[2]), 1)
basicData['Period'] = {}
basicData['Period']['data'] = current.strftime("%Y %B")
basicData['Period']['showKey'] = True
basicData['Period']['showValue'] = True

fin_holidays = holidays.Finland()

data = sys.argv[1:]

calen = calendar.Calendar()
calen_iter = calen.itermonthdates(int(sys.argv[1]), int(sys.argv[2]))

# get the working days of the month
myWorkDays = []
for c in calen_iter:
    if c.month == int(sys.argv[2]):
        if c in fin_holidays or c.weekday() > 4:
            #print (c)
            pass
        else:
            #print ("NOT HOLIDAY", c)
            myWorkDays.append(c)

percentages = data[2::2]
percentages = [0.01*float(p) for p in percentages]
descriptions = data[3::2]

percentageSum = sum(percentages)
remainder = 1 - percentageSum
if percentageSum < 0.999 or percentageSum > 1.001:
    print("WARNING: percenteges should add up to 100, you had: ", percentageSum)
# add missing part equally to every work
percentages = [float(p)+ remainder/len(percentages) for p in percentages]

print("percentageSum:", percentageSum)

print("percentages", percentages)
print("descriptions", descriptions)

# ok, we have the data, lets write the excel...
# https://stackoverflow.com/questions/43537598/write-strings-text-and-pandas-dataframe-to-excel
report = {
        'date': [],
        'description': [],
        'hours': []
        }

taskCount = 0
taskDayCount = 0
calen_iter = calen.itermonthdates(int(sys.argv[1]), int(sys.argv[2]))
monthlySum = 0.0
for c in calen_iter:
    if c.month == int(sys.argv[2]):
        if c in fin_holidays or c.weekday() > 4:
            print (c)
            report['date'].append(c.day)
            report['description'].append(None)
            report['hours'].append(None)
        else:
            print ("NOT HOLIDAY", c)
            report['date'].append(c.day)
            report['description'].append(descriptions[taskCount])
            report['hours'].append(basicData['dailyHours']['data'])
            monthlySum = monthlySum + float(basicData['dailyHours']['data'])
            taskDayCount = taskDayCount + 1
            print(taskDayCount/len(myWorkDays) , percentages[taskCount])
            if taskDayCount/len(myWorkDays) >= percentages[taskCount]:
                #next task
                taskCount = taskCount + 1
                taskDayCount = 0

# sum row
report['date'].append(None)
report['description'].append(None)
report['hours'].append(None)

report['date'].append(None)
report['description'].append("SUM:")
report['hours'].append(monthlySum)

df = pd.DataFrame(report)
sf = StyleFrame(df)
fileName = sys.argv[1]+'-'+sys.argv[2]+'-'+basicData['Employee']['data'].replace(' ', '')+'.xlsx'
#excel_writer = StyleFrame.ExcelWriter('example.xlsx')

writer = StyleFrame.ExcelWriter(fileName)
# count how many title rows
titleRows = 1
for row in basicData:
    if basicData[row]['showKey'] or basicData[row]['showValue']:
        titleRows = titleRows + 1
sf.to_excel(excel_writer=writer, startrow=titleRows, startcol=0, index=False, best_fit='description')

#df.to_excel(writer, startrow=titleRows, startcol=0, index=False)
worksheet = writer.sheets['Sheet1']
# how many title rows we have:
# titles
titleRows = 1
for row in basicData:
    if basicData[row]['showKey'] or basicData[row]['showValue']:
        print(row, basicData[row])
        if basicData[row]['showKey']:
            worksheet.cell(row=titleRows, column=1).value = row
        if basicData[row]['showValue']:
            worksheet.cell(row=titleRows, column=2).value = basicData[row]['data']
        titleRows = titleRows + 1

#text1 = "some text here"
#text2 = "other text here"
#df = pd.DataFrame({"a": [1,2,3,4,5], "b": [6,7,8,9,10], "c": [11,12,13,14,15]})


#another solution
#worksheet.write_string(0, 0, text1)
#worksheet.write_string(1, 0, text2)
writer.save()
