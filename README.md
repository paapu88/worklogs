# Work Logs

- Based on one line of command: automatically generate excel sheet of your monhtly hours for your employer.
- **CHANGE CONTENT OF data.json**: Constant info is read from data.json (your name,  company name etc...)
```
python3 worklogs.py 2019 4 5 "Study of word recognition" 50 "Integrating neural nets to Duranc engine" 45 "Integrating analysers to Duranc engine"
```
Arguments:
 -year
 -month
 
 then
 
 work1 percentage1
 work1 description1
 
 work2 percentage2
 work2 description2
 
 ... etc ...
 
 ![](workLogDemo.png)
 